﻿country_decisions = {

	# Form Makris, ancient name for Euboea, apparently pre-Greek
	form_one_euboia2 = { 
		
		potential = { 
			OR = {	#got this from the Albion file. I think it's to ease lag?
				is_ai = no
				num_of_cities >= 10
			}
			NOT = {
				tag = 1EU
			}
			NOR = { 
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes 
			}
			capital_scope = {
				OR = {
					is_in_area = artemisio_area
					is_in_area = chalkis_area
					is_in_area = karystos_area
				}
			}				 
			NOT = { country_culture_group = hellenic }
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 406
					province_id = 449
					province_id = 366
					province_id = 418
					province_id = 430
					province_id = 411
					province_id = 460
					province_id = 475
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1EU
					}
				}
			}
			can_form_nation_trigger = yes 
			any_owned_province = {
				OR = {
					province_id = 406
					province_id = 449
					province_id = 366
					province_id = 418
					province_id = 430
					province_id = 411
					province_id = 460
					province_id = 475
				}
				count >= 6
			}
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			any_owned_province = {
				OR = {
					province_id = 406
					province_id = 449
					province_id = 366
					province_id = 418
					province_id = 430
					province_id = 411
					province_id = 460
					province_id = 475
				}
				count >= 6
			}
		}
		
		effect = {
			change_country_name = "ONE_EUBOIA2_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_small_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_EUBOIA2_ADJECTIVE"
				change_country_tag = 1EU
				change_country_color = dark_purple
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_area = artemisio_area
							is_in_area = chalkis_area
							is_in_area = karystos_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 