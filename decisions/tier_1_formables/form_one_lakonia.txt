﻿country_decisions = {

	# Form Lakonia
	form_one_lakonia = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1LA
			}
			NOR = { 
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes 
			}
			capital_scope = {
				OR = {
					is_in_area = lakonia_area
					is_in_area = taenaron_cape_area
					is_in_area = kythera_area
					province_id = 131 # southern thyrea
					province_id = 129
					province_id = 130
					province_id = 128
					province_id = 127
				}
				NOT = {
					province_id = 28 # excludes Paralia
					province_id = 29
					province_id = 124 # excludes Kythera
					province_id = 122
					province_id = 123
				}
			}				 
			# primary_culture = helladic not needed imho
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 4
					province_id = 11
					province_id = 18
					province_id = 125
					province_id = 118
					province_id = 113
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1LA
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 4
			owns_or_subject_owns = 11
			owns_or_subject_owns = 18
			owns_or_subject_owns = 125
			owns_or_subject_owns = 118
			owns_or_subject_owns = 113
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 4
			owns_or_subject_owns = 11
			owns_or_subject_owns = 18
			owns_or_subject_owns = 125
			owns_or_subject_owns = 118
			owns_or_subject_owns = 113
		}
		
		effect = {
			change_country_name = "ONE_LAKONIA_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_small_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_LAKONIA_ADJECTIVE"
				change_country_tag = 1LA
				change_country_color = spartan_rust
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_area = lakonia_area
							is_in_area = taenaron_cape_area
							is_in_area = kythera_area
							is_in_area = thyrea_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 