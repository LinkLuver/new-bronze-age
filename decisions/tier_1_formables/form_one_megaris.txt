﻿country_decisions = {

	# Form Megaris
	form_one_megaris = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1MG
			}
			NOR = { 
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes 
			}
			capital_scope = {
				OR = {
					is_in_area = korinthos_area
					is_in_area = athens_area
				}
				NOT = {
					province_id = 239 # excludes Korinthos
					province_id = 249 # excludes salamis
					province_id = 248
					province_id = 258 # excludes Athens
					province_id = 256
					province_id = 257
					province_id = 259
					province_id = 261
				}
			}				 
			# primary_culture = helladic not needed imho
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 246
					province_id = 325
					province_id = 254
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1MG
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 246
			owns_or_subject_owns = 325
			owns_or_subject_owns = 254
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 246
			owns_or_subject_owns = 325
			owns_or_subject_owns = 254
		}
		
		effect = {
			change_country_name = "ONE_MEGARIS_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_small_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_MEGARIS_ADJECTIVE"
				change_country_tag = 1MG
				change_country_color = theban_green
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_area = korinthos_area
							is_in_area = athens_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 