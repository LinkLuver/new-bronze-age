﻿country_decisions = {

	# Form Argolis
	form_one_argolis = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1AG
			}
			NOR = { 
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes 
			}
			capital_scope = {
				OR = {
					is_in_area = thyrea_area
					is_in_area = tiryns_area
					is_in_area = ermione_area
					province_id = 139 #East Tegea
					province_id = 138
					province_id = 205 #southern Aroania
					province_id = 207
					province_id = 206
				}
				NOT = {
					province_id = 235 #excludes Korfos
					province_id = 234
					province_id = 226 #excludes two islands
					province_id = 692
				}
			}
			# primary_culture = helladic not needed imho
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 129
					province_id = 216
					province_id = 218
					province_id = 223
					province_id = 227
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1AG
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 129
			owns_or_subject_owns = 216
			owns_or_subject_owns = 218
			owns_or_subject_owns = 223
			owns_or_subject_owns = 227
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 129
			owns_or_subject_owns = 216
			owns_or_subject_owns = 218
			owns_or_subject_owns = 223
			owns_or_subject_owns = 227
		}
		
		effect = {
			change_country_name = "ONE_ARGOLIS_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_small_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_ARGOLIS_ADJECTIVE"
				change_country_tag = 1AG
				change_country_color = pure_white
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_area = thyrea_area
							is_in_area = tiryns_area
							is_in_area = ermione_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 