﻿country_decisions = {
	
	# Form secret country, meant to be an easter egg or a crazy challenge
	form_one_legend = {
		potential = {
			NOT = {
				tag = 1AT
			}
			NOT = {
				is_endgame_tag_trigger = yes
			}
			tag = THR #a reference to the "Atlantis is Santorini" theory
			p:1036 = {
				is_capital = yes
				total_population >= 40
				owner = root
			}
			num_of_cities >= 100 #this decision remains hidden until you've risen to major power as to keep it a surprise.
		}

		highlight = { 
			scope:province = { province_id = 1036 }
		}
		
		allow = {
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1AT
					}
				}
			}
			can_form_nation_trigger = yes
			p:1036 = {	#should probably add a tooltip for it
				total_population >= 80
			}
			num_of_cities >= 500
		}
		
		ai_allow = {
			num_of_cities >= 500 #the number of territories needed to become a great power currently. It's a pretty crazy number for the mod, when you consider that Crete itself is barely 100 territories, and it might be downscaled later on.
			p:1036 = {	#should probably add a tooltip for it
				total_population >= 80
			}
		}
		
		effect = {
			hidden_effect = {
				change_country_name = "ONE_LEGEND_NAME" #I put the name in hidden to keep the surprise till the end.
				change_country_adjective = "ONE_LEGEND_ADJECTIVE"
				change_country_tag = 1AT
				change_country_color = navy_blue
				# change_country_flag = will need a flag later
			}
			capital_scope = {
				capital_formable_large_effect = yes
				formable_capital_modifier_large_effect = yes
			} 			#could think of cooler boons like a civilization boost, ideal fraction, pop capity and promotion and so on. Anything that would turn the three territories of Thera into a mini-Atlantis, essentially. The bonuses might as well be OP as theyre the kind of thing that, if you've come all the way to unlocking it, you dont really need it anymore by that point.
			p:1036 = {
				add_civilization_value = 10
				}
			p:1037 = {
				add_civilization_value = 10
				}
			p:1035 = {
				add_civilization_value = 10
				}
			add_2_free_province_investments = yes
		}
		
		ai_will_do = {
			base = 1
		}
	}
} 

