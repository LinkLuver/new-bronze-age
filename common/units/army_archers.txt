﻿archers = { #Archers
	army = yes
	assault = yes

	levy_tier = basic

	maneuver = 2
	movement_speed = 2.5

	light_infantry = 1.25
	camels = 0.9
	heavy_infantry = 0.9
	heavy_cavalry = 0.8
	archers = 1.0
	chariots = 1.2
	supply_train = 2.0
	
	supply_train = 2.0
	
	attrition_weight = 0.9
	
	strength_damage_done = 1.0
	strength_damage_taken = 1.2
	morale_damage_taken = 1.25

	
	attrition_loss = 0.05
	ai_max_percentage = 15
	build_cost = {
		gold = 8
		manpower = 1
	}
	food_consumption = 0.1
	food_storage = 2.4
}