REB = {
	pattern = "pattern_diagonal_split_01.tga"
	color1 = "phrygia_red"
	color2 = "pitch_black"
}

BAR = {
	pattern = "pattern_solid.tga"
	color1 = "ck2_black"
	color2 = "phrygia_red"

	textured_emblem = {
		texture = "te_skull_goat.dds"
	}
}

MER = {
	pattern = "pattern_solid.tga"
	color1 = "ck2_black"
	color2 = "roman_red"

	textured_emblem = {
		texture = "te_mercenary.dds"
	}
}

PIR = {
	pattern = "pattern_solid.tga"
	color1 = "pitch_black"
	color2 = "bone_white"

	colored_emblem = {
		texture = "ce_pirates.dds"
		color1 = "bone_white"
		color2 = "ck2_green"
	}
}
WAS = {
    pattern = "pattern_solid.tga"
    color1 = "frost_blue"
    color2 = "roman_gold"

    colored_emblem = {
        texture = "ce_waset_staff.dds"
        color1 = "roman_gold"
        instance = { scale = { 0.75 0.75 }  }
    }

    colored_emblem = {
        texture = "ce_border_rug_01.tga"
        color1 = "roman_gold"
        color2 = "frost_blue"
        instance = { rotation = 0 }
        instance = { rotation = 180 }
    }
}
HNN = {
    pattern = "pattern_solid.tga"
    color1 = "roman_red"
    color2 = "roman_gold"

    colored_emblem = {
        texture = "ce_heryshaf.dds"
        color1 = "roman_gold"
        instance = { scale = { 0.75 0.75 }  }
    }

    colored_emblem = {
        texture = "ce_border_rug_01.tga"
        color1 = "roman_gold"
        color2 = "roman_red"
        instance = { rotation = 0 }
        instance = { rotation = 180 }
    }
}
KNO = {
    pattern = "pattern_solid.tga"
    color1 = "roman_red"
    color2 = "roman_red"

    colored_emblem = {
        texture = "ce_bull_02.dds"
        color1 = "ck2_black"
        instance = { scale = { 0.75 0.75 }  }
    }

    colored_emblem = {
        texture = "ce_border_rug_01.tga"
        color1 = "ck2_black"
        color2 = "roman_red"
        instance = { rotation = 0 }
        instance = { rotation = 180 }
    }
}
GUB = {
    pattern = "pattern_solid.tga"
    color1 = rgb { 140 30 10 }
    color2 = rgb { 110 10 140 }

    colored_emblem = {
        texture = "ce_byblos.dds"
        color1 = "roman_gold"
    }
}
THE = {
    pattern = "pattern_solid.tga"
    color1 = "thebes_background_color"
    color2 = "thebes_background_color"

    colored_emblem = {
        texture = "ce_seven_gates.dds"
        color1 = "thebes_secondary_color"
        color2 = "thebes_main_color"
    }
}
URR = {
    pattern = "pattern_solid.tga"
    color1 = "pitch_black"
    color2 = "galatia_light_blue_color"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_1.dds"
        color1 = "galatia_light_blue_color"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_ur.dds"
        color1 = "galatia_light_blue_color"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

URK = {
    pattern = "pattern_solid.tga"
    color1 = "bone_white"
    color2 = "spartan_rust"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_2.dds"
        color1 = "spartan_rust"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_uruk.dds"
        color1 = "spartan_rust"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

LGS = {
    pattern = "pattern_solid.tga"
    color1 = "light_purple"
    color2 = "samnite_beige"

    colored_emblem = {
        texture = "ce_lagash_eagle.dds"
        color1 = "samnite_beige"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_lagash.dds"
        color1 = "ck2_black"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

SNN = {
    pattern = "pattern_solid.tga"
    color1 = "dark_green"
    color2 = "syracuse_yellow"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_5.dds"
        color1 = "syracuse_yellow"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_isin.dds"
        color1 = "syracuse_yellow"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

KSH = {
    pattern = "pattern_solid.tga"
    color1 = "ck2_white"
    color2 = "pink"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_4.dds"
        color1 = "pink"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_kish.dds"
        color1 = "ck2_black"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

BBL = {
    pattern = "pattern_solid.tga"
    color1 = "roman_red"
    color2 = "pure_white"

    colored_emblem = {
        texture = "ce_ishtar_lion.dds"
        color1 = "samnite_beige"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_babylon.dds"
        color1 = "samnite_beige"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

SPR = {
    pattern = "pattern_solid.tga"
    color1 = "navy_blue"
    color2 = "pure_white"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_3.dds"
        color1 = "pure_white"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_sippar.dds"
        color1 = "pure_white"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

GTM = {
    pattern = "pattern_solid.tga"
    color1 = "pitch_black"
    color2 = "roman_gold"

    colored_emblem = {
        texture = "ce_mesopatamian_weapons_4.dds"
        color1 = "roman_gold"
        color2 = "blood_red"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_gutium.dds"
        color1 = "roman_gold"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

AKKAD_FLAG = {
    pattern = "pattern_solid.tga"
    color1 = "aestuia_color"
    color2 = "light_gold"

    colored_emblem = {
        texture = "ce_mesopatamian_hat_1.dds"
        color1 = "light_gold"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_akkad.dds"
        color1 = "light_gold"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}
ASR = {
    pattern = "pattern_solid.tga"
    color1 = "ck2_white"
    color2 = "persia_color"

    colored_emblem = {
        texture = "ce_lamassu.dds"
        color1 = "persia_color"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_assur.dds"
        color1 = "persia_color"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

MRI = {
    pattern = "pattern_solid.tga"
    color1 = "judea_blue"
    color2 = "bright_yellow"

    colored_emblem = {
        texture = "ce_thunder_symbol.dds"
        color1 = "bright_yellow"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.44 } }
    }

    colored_emblem = {
        texture = "ce_mari.dds"
        color1 = "bright_yellow"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

USH = {
    pattern = "pattern_solid.tga"
    color1 = "hib_color"
    color2 = "mint_green"

    colored_emblem = {
        texture = "ce_mesopatamian_astrological_6.dds"
        color1 = "mint_green"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_urkesh.dds"
        color1 = "mint_green"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}
LLB = {
    pattern = "pattern_solid.tga"
    color1 = "phokaian_blue"
    color2 = "ck2_yellow"

    colored_emblem = {
        texture = "ce_mesopatamian_weapons_1.dds"
        color1 = "ck2_yellow"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_lullubum.dds"
        color1 = "ck2_yellow"
    }
}

SRM = {
    pattern = "pattern_solid.tga"
    color1 = "ck2_white"
    color2 = "ck2_black"

    colored_emblem = {
        texture = "ce_mesopatamian_weapons_2.dds"
        color1 = "ck2_black"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_simurrum.dds"
        color1 = "ck2_black"
    }
}

SUS = {
    pattern = "pattern_solid.tga"
    color1 = "mint_green"
    color2 = "rust_brown"

    colored_emblem = {
        texture = "ce_palm_symbol.dds"
        color1 = "rust_brown"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_susan.dds"
        color1 = "rust_brown"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}
NSH = {
    pattern = "pattern_solid.tga"
    color1 = "offwhite"
    color2 = "cilicia_color"

    colored_emblem = {
        texture = "ce_cross_symbol.dds"
        color1 = "cilicia_color"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.44 } }
    }

    colored_emblem = {
        texture = "ce_anshan.dds"
        color1 = "cilicia_color"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.48 } }
    }
}

DMN = {
    pattern = "pattern_solid.tga"
    color1 = "bharatavarsha_orange_color"
    color2 = "bone_white"

    colored_emblem = {
        texture = "ce_sumerian_goat.dds"
        color1 = "bone_white"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_dilmun.dds"
        color1 = "pitch_black"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}

MMA = {
    pattern = "pattern_solid.tga"
    color1 = "bone_white"
    color2 = "ck2_black"

    colored_emblem = {
        texture = "ce_mesopatamian_weapons_3.dds"
        color1 = "ck2_black"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_umma.dds"
        color1 = "ck2_black"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.48 } }
    }
}

DER = {
    pattern = "pattern_solid.tga"
    color1 = "bone_white"
    color2 = "armorica_color"

    colored_emblem = {
        texture = "ce_spiral_snake.dds"
        color1 = "armorica_color"
        instance = { scale = { 0.90 0.90 }  position = { 0.50 0.45 } }
    }

    colored_emblem = {
        texture = "ce_der.dds"
        color1 = "armorica_color"
        instance = { scale = { 1.10 1.10 }  position = { 0.50 0.47 } }
    }
}