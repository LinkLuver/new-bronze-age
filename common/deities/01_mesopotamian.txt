﻿#Idea: Use Sukkal's, personal servants of the God's, as treasures, like "Stone of Nirah" 
deity_ishtar = { #Ishtar, Sumerian Inanna
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ishtar = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_population_capacity_modifier = deity_global_population_capacity_modifier_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_inanna = { #Inanna, Uruk, Akkadian Ishtar
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_inanna = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_population_capacity_modifier = deity_global_population_capacity_modifier_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_enlil = { #Enlil, Nippur, Most important God of Wind air earth and storms
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_enlil = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war 
	passive_modifier = { experience_decay = deity_experience_decay_svalue }
	omen = { manpower_recovery_speed = omen_manpower_recovery_speed } 
	religion = mesopotamian_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_nanna = { #Nanna, Ur, Moon God, (Akkadian Sin)
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nanna = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { global_pop_assimilation_speed_modifier = deity_global_pop_assimilation_speed_modifier_svalue }
	omen = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_sin = { #Sin, Moon God, Sumerian Nanna
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_sin = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { global_pop_assimilation_speed_modifier = deity_global_pop_assimilation_speed_modifier_svalue }
	omen = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_nergal = { #Nergal, Kutha/Mashkan-Shapir, God of the underworld, Kutha
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nergal = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war 
	passive_modifier = { army_maintenance_cost = deity_army_maintenance_cost_svalue }
	omen = { land_morale_modifier = omen_land_morale_modifier_svalue } 
	religion = mesopotamian_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = { #Keep this effect
		military_apotheosis_capital_freemen_effect = yes
	}
}
deity_nabu = { #Nabu, Borsippa, God of Scribes, Sumerian Nisaba
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		NOT = { primary_culture = dilmunite }
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nabu = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth 
	passive_modifier = { research_points_modifier = deity_research_points_modifier_svalue }
	omen = { global_citizen_happyness = omen_global_citizen_happiness_svalue } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_nisaba = { #Nisaba, Umma, God of Scribes, Akkadian Nabu
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		NOT = { primary_culture = dilmunite }
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nisaba = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth 
	passive_modifier = { research_points_modifier = deity_research_points_modifier_svalue }
	omen = { global_citizen_happyness = omen_global_citizen_happiness_svalue } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_marduk = { #Marduk, Babylon, 
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		tag = BBL
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_marduk = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	omen = { ruler_popularity_gain = omen_ruler_popularity_gain_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_slaves_effect = yes
	}
}
deity_ninurta = { #Ninurta, Girsu (New) (Old, Lagash (bcs Gudea of Lagash)), God of Farming and healing 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ninurta = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { global_food_capacity = deity_global_food_capacity_svalue }
	omen = { global_monthly_food_modifier = omen_global_monthly_food_modifier } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_food_effect = yes
	}
}
deity_ashur = { #Ashur, Ashur 
	trigger = {
		polytheistic_check_religion_trigger = yes
		primary_culture = assyrian
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ashur = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	omen = { global_monthly_civilization = omen_global_monthly_civilization_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_enki = { #Enki, Eridu, God of Water, knowledge etc
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_enki = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { navy_maintenance_cost = deity_navy_maintenance_cost_svalue }
	omen = { ship_repair_at_sea = deity_ship_repair_at_sea } 
	religion = mesopotamian_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}
deity_ea = { #Ea, God of Water, knowledge etc
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ea = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war 
	passive_modifier = { navy_maintenance_cost = deity_navy_maintenance_cost_svalue }
	omen = { ship_repair_at_sea = deity_ship_repair_at_sea }
	religion = mesopotamian_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}
deity_dumuzid = { #Dumuzid, Bad-tibira God of the Shepherd consort of Inanna, Akkadian Tammuz
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_dumuzid = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth 
	passive_modifier = { global_tribesmen_happyness = global_tribesmen_happiness_svalue }
	omen = { global_tribesmen_output = omen_global_tribesmen_output_svalue } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_tammuz = { #Tammuz, God of the Shepherd consort of Ishtar, Sumerian Dumuzid
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_tammuz = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth 
	passive_modifier = { global_tribesmen_happyness = global_tribesmen_happiness_svalue }
	omen = { global_tribesmen_output = omen_global_tribesmen_output_svalue }
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_ereshkigal = { #Ereshkigal, Goddess of the Underworld consort of Nergal
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ereshkigal = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { stability_monthly_change = deity_stability_monthly_change }
	omen = { cohort_reinforcement_speed = deity_cohort_reinforcement_speed_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_nintinugga = { #Nintinugga,A Goddess of healing, consort of Ninurta, Akkadian Bau
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nintinugga = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { manpower_recovery_speed = deity_global_manpower_recovery_speed_svalue }
	omen = { ruler_popularity_gain = omen_ruler_popularity_gain_svalue } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_bau = { #Bau,A Goddess of healing, consort of Ninurta, Sumerian Nintinugga
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_bau = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { manpower_recovery_speed = deity_global_manpower_recovery_speed_svalue }
	omen = { ruler_popularity_gain = omen_ruler_popularity_gain_svalue } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_ishkur = { #Ishkur,God of Storm and rain, Akkadian Adad
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ishkur = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { happiness_for_same_culture_modifier = deity_happiness_for_wrong_culture_modifier_svalue }
	omen = { naval_damage_taken = omen_naval_damage_taken_svalue } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_income_effect = yes
	}
}
deity_adad = { #Adad,God of Storm and rain, Sumerian Ishkur
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_adad = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth 
	passive_modifier = { happiness_for_same_culture_modifier = deity_happiness_for_wrong_culture_modifier_svalue }
	omen = { naval_damage_taken = deity_naval_damage_taken_svalue }  
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_income_effect = yes
	}
}
deity_ishtaran = { #Ishtaran,Local God of Der, similar to Dumuzid
	trigger = {
		polytheistic_check_religion_trigger = yes
		tag = DER
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ishtaran = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_capital_trade_routes = deity_global_capital_trade_routes_svalue }
	omen = { build_cost = omen_build_cost_svalue } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_ninazu = { #Inazu, Enegi, associated with underworld son of Ereshkigal
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ninazu = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_slaves_happyness = deity_global_slaves_happiness_svalue }
	omen = { price_local_civ_button_cost_modifier = omen_global_monthly_civilization_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_capital_effect = yes
	}
}
deity_tishpak = { #Tishpak, Eshnunna,Syncreticism of the Hurrian storm god Teshub
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_tishpak = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { monthly_experience_gain = deity_monthly_experience_gain_svalue }
	omen = { discipline = omen_discipline_svalue } 
	religion = mesopotamian_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_ninlil = { #Ninlil, (Dilmun, not anymore), Sumerian Goddess, Mother of lots of God's, Assyrian Mulliltu
	trigger = {
		polytheistic_check_religion_trigger = yes
		NOT = { primary_culture = assyrian }
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ninlil = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence 
	passive_modifier = { diplomatic_relations = deity_diplomatic_relations_svalue }
	omen = { monthly_wage_modifier = omen_monthly_wage_modifier_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_mulliltu = { #Mulliltu, Mother of lots of God's, Sumerian Ninlil
	trigger = {
		polytheistic_check_religion_trigger = yes
		primary_culture = assyrian
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_mulliltu = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { diplomatic_relations = deity_diplomatic_relations_svalue }
	omen = { monthly_wage_modifier = omen_monthly_wage_modifier_svalue } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_ninhursag = { #Ninhursag, Adab,"Lady of the Sacred Mountain" fertility Goddess, Akkadian Belet-Ili, lots of Kings used her name
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		NOT = { primary_culture = dilmunite }
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ninhursag = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { global_freemen_happyness = deity_global_freemen_happyness_svalue }
	omen = { diplomatic_reputation = deity_diplomatic_reputation_svalue  } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_belet_ili = { #Belet-Ili,"Lady of the Sacred Mountain" fertility Goddess, Sumerian Ninhursag
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_belet_ili = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { global_freemen_happyness = deity_global_freemen_happyness_svalue }
	omen = { diplomatic_reputation = deity_diplomatic_reputation_svalue  } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
#New
deity_utu = { #Utu, Sippar/Larsa, " Sun god - god of justice, morality, and truth, and the twin of the goddess Inanna, the Queen of Heaven." "Utu's main personality characteristics are his kindness and generosity"
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_utu = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { stability_monthly_change = deity_stability_monthly_change }
	omen = { global_slaves_happyness = omen_global_slaves_happiness_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_shamash = { #Shamash, " Sun god - god of justice, morality, and truth, and the twin of the goddess Inanna, the Queen of Heaven." "Utu's main personality characteristics are his kindness and generosity" Akkadian Utu
	trigger = {
		polytheistic_check_religion_trigger = yes
		sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_shamash = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { stability_monthly_change = deity_stability_monthly_change }
	omen = { global_slaves_happyness = omen_global_slaves_happiness_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_ningal = { #Ningal, Ur and Harran(later), "a goddess of reeds" "She is chiefly recognised at Ur, and was probably first worshipped by cow-herders in the marsh lands of southern Mesopotamia"
	trigger = {
		polytheistic_check_religion_trigger = yes
		#sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_ningal = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_tribesmen_output = global_tribesmen_output_svalue }
	omen = { global_slaves_happyness = omen_global_slaves_happiness_svalue } 
	religion = mesopotamian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_geshtinanna = { #Geshtinanna, Isin(Nippur,Uruk), "agriculture, fertility, and dream interpretation"
	trigger = {
		polytheistic_check_religion_trigger = yes
		#sumerian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_geshtinanna = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { happiness_for_same_religion_modifier = deity_happiness_for_same_religion_modifier }
	omen = { global_monthly_civilization = omen_global_monthly_civilization_svalue  } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_nanshe = { #Nanshe, Lagash, Local Deity of Lagash, Daughter of Enki and Ninhursag, Goddes of Social Justice, prophecy, fertility and fishing. "Like her father, she was heavily associated with water. She held dominion over the Persian Gulf and all the animals within. Her seat of power was the Sirara temple, located in the city of Lagash."
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			rare_deity_trigger = {
				RELIGION = mesopotamian_religion
			}
			deity:omen_nanshe = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_commerce_modifier = deity_global_commerce_modifier_svalue }
	omen = { happiness_for_same_culture_modifier = deity_happiness_for_wrong_culture_modifier_svalue  } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}
deity_inzak = { #Inzak, Failaka, Identified with Nabu by the Babylonians
	trigger = {
		polytheistic_check_religion_trigger = yes
		#akkadian_pantheon_trigger = no
		OR = {
			common_deity_culture_trigger = {
				RELIGION = mesopotamian_religion
				CULTURE = dilmunite
			}
			deity:omen_inzak = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love 
	passive_modifier = { global_freemen_happyness = deity_global_freemen_happyness_svalue }
	omen = { diplomatic_reputation = deity_diplomatic_reputation_svalue  } 
	religion = mesopotamian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_meskilak = { #Meskilak, Dilmun, "Meskilak is the patron goddess of the city of Dilmun.[254] She may have been seen as the wife or mother of Inzak.[254] The Sumerians seem to have identified her with Ninhursag.[254] She is sometimes referred to as Nin-Dilmun, meaning "Lady of Dilmun"."
	trigger = {
		polytheistic_check_religion_trigger = yes
		akkadian_pantheon_trigger = no
		OR = {
			common_deity_culture_trigger = {
				RELIGION = mesopotamian_religion
				CULTURE = dilmunite
			}
			deity:omen_meskilak = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_commerce_modifier = deity_global_commerce_modifier_svalue }
	omen = { happiness_for_same_culture_modifier = deity_happiness_for_wrong_culture_modifier_svalue  } 
	religion = mesopotamian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}