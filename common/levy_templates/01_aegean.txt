﻿# Scriptable Levy template, used to determine levy composition for different cultures.
#
#	NOTE: All numbers are relative percentages, ex. 0.2 means that 20% of this template should be a certain tier or unit
#	2nd NOTE: Support units will be added automatically in code when a certain size of Levy is raised, replacing another unit
#	3rd NOTE: If a specific subunit cannot be constructed for some reason another one that can from that tier will replace it ( if possible )
#

levy_aegean = {	#Poleis with Hoplites
	default = no

	light_infantry = 0.5
	camels = 0.1
	archers = 0.4
}

levy_cretan = {
	default = no
	
	heavy_infantry = 0.2
	light_infantry = 0.4
	camels = 0.2
	archers = 0.2
}

levy_phrygian = {
	default = no

	light_infantry = 0.6
	archers = 0.4
}


levy_greek = {
	default = no
	
	light_infantry = 0.4
	archers = 0.4
	heavy_infantry = 0.2
}