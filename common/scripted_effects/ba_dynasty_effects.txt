﻿dynasty_init_effect = {
	if = {
		limit = { NOT = { tag = REB } }
		add_to_global_variable_list = {
			name = egyptian_dynasties
			target = this
		}
		save_scope_as = new_dynasty
		#custom_tooltip = "Gain a Claim on all of Egypt"
		hidden_effect = {
			if = {
				limit = {
					root = {
						is_ai = no
					}
				}
				every_area = {
					limit = {
						any_area_province = {
							is_in_egypt_trigger = yes
						}
					}
					every_area_province = {
						limit = {
							NOT = { owned_or_subject_owned = scope:new_dynasty }
						}
						add_claim = scope:new_dynasty
					}
				}
			}
		}
	}
}
set_player_dynasties_list_effect = {
	every_country = {
		limit = {
			is_ai = no
		}
		save_scope_as = targetcountry
		clear_variable_list = egyptian_dynasties
		every_in_global_list = {
			variable = egyptian_dynasties

			scope:targetcountry = {
				add_to_variable_list = {
					name = egyptian_dynasties
					target = prev
				}
			}
		}
	}
}
found_egyptian_dynasty_effect = {
	custom_tooltip = "YOU_FOUND_NEW_DYNASTY_TT"
	add_political_influence = influence_large
	switch_government_type_event_clearup_effect = yes
	every_country = {
		limit = {
			government = egyptian_dynasty
		}
		add_opinion = { modifier = rival_dynasty target = prev}
		reverse_add_opinion = { modifier = rival_dynasty target = prev}
	}
	change_government = egyptian_dynasty
	dynasty_init_effect = yes
}