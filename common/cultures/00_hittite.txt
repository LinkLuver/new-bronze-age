﻿hittite = {	
	color = hsv { 0.1 0.9 0.8 }
	primary = light_infantry
	second = archers
	flank = chariot

	primary_navy = tetrere
	secondary_navy = octere
	flank_navy = liburnian
	
	levy_template = levy_hittit


	male_names = { # Add names for hittite
		Anittas Arnuwandas Hattusilis Katuzili Lubarna Mursilis Mutalla Muwatallis Piyamaradu Saplulme Suppiluliuma Telipinus Tudhaliyas Abdihepa Tatuhepa Abutesup Akia Akitesup Akiizzi Alaksandu Amaiase Anuwaanza Arnuanta Artamaania Artataama Artaassumara Artesupa Arzaia Asali Atamu Bantisinni Barbuiluwa Ariaenni Etagama Giliia Giluhepa Halpasulubi Hibia Initesup Eniel Irsappa Katihutisupa Peruwa Galulu Saktanuwa Suppiahsu
	}
	female_names = {
		Pudukhepa Labbaia Lieia Hepatuzzi Walawala Allaiturahi Ziza Hatasusar Hati Manna Waliwali Ammi Satahsusar Paskuwa Asupnawiya Kupapa Muwatti Arsakiti Tiwatawiya Zuskana Kipi Asdu Wasti Hasusra Nasazi Hahaluwan Manniya Ilalilka Asmunikal Hapuwahsusar Kuwattalla Aliwanatti Halwati
	}
	family = {
		Ahmid.Ahmid.Ahmid.Ahmid
		Galestus.Galestid.Galestid.Galestid
		Kenamus.Kenamid.Kenamid.Kenamid
		Naravid.Naravid.Naravid.Naravid
		Penamus.Penamid.Penamid.Penamid
		Senuid.Senuid.Senuid.Senuid
		Setnid.Setnid.Setnid.Setnid
		Arganid.Arganid.Arganid.Arganid
		Psammid.Psammid.Psammid.Psammid
		Philothid.Philothid.Philothid.Philothid
		Timolid.Timolid.Timolid.Timolid
		Zosid.Zosid.Zosid.Zosid
		Phileid.Phileid.Phileid.Phileid
		Nicodemid.Nicodemid.Nicodemid.Nicodemid
		Nefarid.Nefarid.Nefarid.Nefarid
		Bastid.Bastid.Bastid.Bastid
		Xeneid.Xeneid.Xeneid.Xeneid
		Naravid.Naravid.Naravid.Naravid
		Ezanid.Ezanid.Ezanid.Ezanid
		Ahmid.Ahmid.Ahmid.Ahmid
		Alarus.Alara.Alarid.Alarid
		Harsiotes.Harsiotes.Harsiotid.Harsiotid
		Henus.Hena.Henid.Henid
		Ibus.Iba.Ibid.Ibid
		Khabbash.Khabbash.Khabbashid.Khabbashid	
		Amasus.Amasa.Amasid.Amasid
		Nectanebo.Nectanebo.Nectanebid.Nectanebid
		Petubast.Petubast.Petubastes.Petubastid
		Djedhid.Djedhid.Djedhid.Djedhid
		Tachys.Tachya.Tachyes.Tachyid
		Mago.Mago.Mago.Magid	
	}
	culture = {
		nesili = {} # Indo-european maybe group with luwian
	}
	
	barbarian_names = { 
		hittite_barb
	}
	
	graphical_culture = celtic_gfx	
	ethnicities = {
		10 = celtic
	}
}