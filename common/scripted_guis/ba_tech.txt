﻿palatial_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = palatial_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = palatial
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = palatial
		}
	}
}
has_palatial_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = palatial_2
			has_country_modifier = palatial_3
			has_country_modifier = palatial_4
		}
	}
}
has_palatial_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = palatial_3
			has_country_modifier = palatial_4
		}
	}
}
has_palatial_4 = {
	scope = country
	is_shown = {
		has_country_modifier = palatial_4
	}
}
#Writing
writing_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = writing_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = writing
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = writing
		}
	}
}
has_writing_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = writing_2
			has_country_modifier = writing_3
			has_country_modifier = writing_4
		}
	}
}
has_writing_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = writing_3
			has_country_modifier = writing_4
		}
	}
}
has_writing_4 = {
	scope = country
	is_shown = {
		has_country_modifier = writing_4
	}
}
urbanization_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = urbanization_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = urbanization
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = urbanization
		}
	}
}
has_urbanization_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = urbanization_2
			has_country_modifier = urbanization_3
			has_country_modifier = urbanization_4
		}
	}
}
has_urbanization_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = urbanization_3
			has_country_modifier = urbanization_4
		}
	}
}
has_urbanization_4 = {
	scope = country
	is_shown = {
		has_country_modifier = urbanization_4
	}
}
military_structure_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = military_structure_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = military_structure
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = military_structure
		}
	}
}
has_military_structure_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = military_structure_2
			has_country_modifier = military_structure_3
			has_country_modifier = military_structure_4
		}
	}
}
has_military_structure_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = military_structure_3
			has_country_modifier = military_structure_4
		}
	}
}
has_military_structure_4 = {
	scope = country
	is_shown = {
		has_country_modifier = military_structure_4
	}
}
trade_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = trade_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = trade
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = trade
		}
	}
}
has_trade_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = trade_2
			has_country_modifier = trade_3
			has_country_modifier = trade_4
		}
	}
}
has_trade_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = trade_3
			has_country_modifier = trade_4
		}
	}
}
has_trade_4 = {
	scope = country
	is_shown = {
		has_country_modifier = trade_4
	}
}
law_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = law_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = law
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = law
		}
	}
}
has_law_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = law_2
			has_country_modifier = law_3
			has_country_modifier = law_4
		}
	}
}
has_law_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = law_3
			has_country_modifier = law_4
		}
	}
}
has_law_4 = {
	scope = country
	is_shown = {
		has_country_modifier = law_4
	}
}
bureaucracy_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = bureaucracy_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = bureaucracy
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = bureaucracy
		}
	}
}
has_bureaucracy_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = bureaucracy_2
			has_country_modifier = bureaucracy_3
			has_country_modifier = bureaucracy_4
		}
	}
}
has_bureaucracy_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = bureaucracy_3
			has_country_modifier = bureaucracy_4
		}
	}
}
has_bureaucracy_4 = {
	scope = country
	is_shown = {
		has_country_modifier = bureaucracy_4
	}
}
irrigation_upgrade_button = {
	scope = country

	ai_is_valid = {
		always = no
	}

	is_shown = {
		NOT = { has_country_modifier = irrigation_4 }
	}
	is_valid = {
		can_increase_specific_tech_trigger = {
			TYPE = irrigation
		}
	}
	effect = {
		increase_civ_tech_effect = {
			TYPE = irrigation
		}
	}
}
has_irrigation_2 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = irrigation_2
			has_country_modifier = irrigation_3
			has_country_modifier = irrigation_4
		}
	}
}
has_irrigation_3 = {
	scope = country
	is_shown = {
		OR = {
			has_country_modifier = irrigation_3
			has_country_modifier = irrigation_4
		}
	}
}
has_irrigation_4 = {
	scope = country
	is_shown = {
		has_country_modifier = irrigation_4
	}
}