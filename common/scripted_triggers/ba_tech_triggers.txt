﻿can_increase_specific_tech_trigger = {
	has_variable = civ_tech
	trigger_if = {
		limit = { has_country_modifier = $TYPE$_4 }
		always = no
	}
	trigger_else_if = {
		limit = { has_variable = civ_tech }
		trigger_if = {
			limit = { 
				NOR = {
					has_country_modifier = $TYPE$_2
					has_country_modifier = $TYPE$_3
				} 
			}
			custom_tooltip = {
				text = "CAN_GET_CIVTECH_2_TT"
				civ_tech_tech_diff_svalue >= 0.5
			}
		}
		trigger_else_if = {
			limit = { has_country_modifier = $TYPE$_2 }
			custom_tooltip = {
				text = "CAN_GET_CIVTECH_3_TT"
				civ_tech_tech_diff_svalue >= 1
			}
		}
		trigger_else_if = {
			limit = { NOT = { has_country_modifier = $TYPE$_4 } }
			custom_tooltip = {
				text = "CAN_GET_CIVTECH_4_TT"
				civ_tech_tech_diff_svalue >= 1.5
			}
		}
	}
}