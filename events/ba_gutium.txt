﻿namespace = ba_gutium

ba_gutium.1 = { #Gutium Lose
    type = country_event
    hidden = yes

    immediate = {
    	p:6126 = { #Akkad
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "AKKAD_NAME"
				#change_country_adjective = "AKKAD_ADJECTIVE"
				set_country_heritage = akkad_heritage
			}
		}
		p:5756 = {
			set_conquered_by = p:6126.owner
		}
		p:5755 = {
			set_conquered_by = p:6126.owner
		}
		p:6650 = {
			set_conquered_by = p:6126.owner
		}
		p:5757 = {
			set_conquered_by = p:6126.owner
		}
		p:6500 = {
			set_conquered_by = p:6126.owner
		}
		p:5899 = {
			set_conquered_by = p:6126.owner
		}
		p:6131 = {
			set_conquered_by = p:6126.owner
		}
		p:6130 = {
			set_conquered_by = p:6126.owner
		}
		p:5898 = {
			set_conquered_by = p:6126.owner
		}
		p:6129 = {
			set_conquered_by = p:6126.owner
		}
		p:6133 = {
			set_conquered_by = p:6126.owner
		}
		p:6408 = {
			set_conquered_by = p:6126.owner
		}
		p:6132 = {
			set_conquered_by = p:6126.owner
		}
		p:6128 = {
			set_conquered_by = p:6126.owner
		}
		p:5753 = { #Eshnunna
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "ESHNUNNA_NAME"
				#change_country_adjective = "ESHNUNNA_ADJECTIVE"
			}
		}
		p:5694 = {
			set_conquered_by = p:5753.owner
		}
		p:5754 = {
			set_conquered_by = p:5753.owner
		}
		p:6247 = {
			set_conquered_by = p:5753.owner
		}
		p:5699 = {
			set_conquered_by = p:5753.owner
		}
		p:5698 = {
			set_conquered_by = p:5753.owner
		}
		p:6237 = {
			set_conquered_by = p:5753.owner
		}
		p:6233 = {
			set_conquered_by = p:5753.owner
		}
		p:5696 = {
			set_conquered_by = p:5753.owner
		}
		p:5695 = {
			set_conquered_by = p:5753.owner
		}
		p:6127 = {
			set_conquered_by = p:5753.owner
		}
		p:5697 = {
			set_conquered_by = p:5753.owner
		}
		p:6651 = {
			set_conquered_by = p:5753.owner
		}
		p:5897 = {
			set_conquered_by = p:5753.owner
		}
		p:5594 = { #Meturna
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "METURNA_NAME"
				#change_country_adjective = "METURNA_ADJECTIVE"
			}
		}
		p:5693 = {
			set_conquered_by = p:5594.owner
		}
		p:5690 = {
			set_conquered_by = p:5594.owner
		}
		p:5692 = {
			set_conquered_by = p:5594.owner
		}
		p:5691 = {
			set_conquered_by = p:5594.owner
		}
		p:5595 = {
			set_conquered_by = p:5594.owner
		}
		p:5591 = {
			set_conquered_by = p:5594.owner
		}
		p:5592 = {
			set_conquered_by = p:5594.owner
		}
		p:5590 = {
			set_conquered_by = p:5594.owner
		}
		p:5599 = {
			set_conquered_by = p:5594.owner
		}
		p:5601 = {
			set_conquered_by = p:5594.owner
		}
		p:5602 = {
			set_conquered_by = p:5594.owner
		}
		p:5603 = {
			set_conquered_by = p:5594.owner
		}
		p:6157 = { #Akshak
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "AKSHAK_NAME"
				#change_country_adjective = "AKSHAK_ADJECTIVE"
			}
		}
		p:6158 = {
			set_conquered_by = p:6157.owner
		}
		p:6241 = {
			set_conquered_by = p:6157.owner
		}
		p:6242 = {
			set_conquered_by = p:6157.owner
		}
		p:6156 = {
			set_conquered_by = p:6157.owner
		}
		p:6136 = {
			set_conquered_by = p:6157.owner
		}
		p:6137 = {
			set_conquered_by = p:6157.owner
		}
		p:6652 = {
			set_conquered_by = p:6157.owner
		}
		p:6210 = {
			set_conquered_by = p:6157.owner
		}
		p:6243 = {
			set_conquered_by = p:6157.owner
		}
		p:6245 = {
			set_conquered_by = p:6157.owner
		}
		p:6135 = { #Push
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "PUSH_NAME"
				#change_country_adjective = "PUSH_ADJECTIVE"
			}
		}
		p:6411 = {
			set_conquered_by = p:6135.owner
		}
		p:6134 = {
			set_conquered_by = p:6135.owner
		}
		p:6410 = {
			set_conquered_by = p:6135.owner
		}
		p:6409 = {
			set_conquered_by = p:6135.owner
		}
		p:6238 = { #First Migratory Tribe
	    	create_country = {
				change_government = tribal_chiefdom
				set_primary_culture = gutian
				set_country_religion = mesopotamian_religion
				#change_country_name = "GUTIAN_MIGRATORY_ONE_NAME"
				#change_country_adjective = "GUTIAN_MIGRATORY_ONE_ADJECTIVE"
			}
		}
		p:6235 = {
			set_conquered_by = p:6238.owner
		}
		p:6232 = {
			set_conquered_by = p:6238.owner
		}
		p:6240 = {
			set_conquered_by = p:6238.owner
		}
		p:6244 = {
			set_conquered_by = p:6238.owner
		}
		p:6246 = {
			set_conquered_by = p:6238.owner
		}
		p:6239 = {
			set_conquered_by = p:6238.owner
		}
		p:6229 = {
			set_conquered_by = p:6238.owner
		}
		p:6224 = {
			set_conquered_by = p:6238.owner
		}
		p:6228 = {
			set_conquered_by = p:6238.owner
		}
		p:6230 = {
			set_conquered_by = p:6238.owner
		}
		p:6231 = {
			set_conquered_by = p:6238.owner
		}
		p:6234 = {
			set_conquered_by = p:6238.owner
		}
		p:6236 = {
			set_conquered_by = p:6238.owner
		}
		p:6211 = { #Second Migratory Tribe
	    	create_country = {
				change_government = tribal_chiefdom
				set_primary_culture = gutian
				set_country_religion = mesopotamian_religion
				#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
				#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
			}
		}
		p:6255 = {
			set_conquered_by = p:6211.owner
		}
		p:6208 = {
			set_conquered_by = p:6211.owner
		}
		p:6254 = {
			set_conquered_by = p:6211.owner
		}
		p:6212 = {
			set_conquered_by = p:6211.owner
		}
		p:6253 = {
			set_conquered_by = p:6211.owner
		}
		p:6214 = {
			set_conquered_by = p:6211.owner
		}
		p:6260 = {
			set_conquered_by = p:6211.owner
		}
		p:6213 = {
			set_conquered_by = p:6211.owner
		}
		p:6227 = {
			set_conquered_by = p:6211.owner
		}
		p:6226 = {
			set_conquered_by = p:6211.owner
		}
		p:6225 = {
			set_conquered_by = p:6211.owner
		}
		p:6435 = { #Third Migratory Tribe
	    	create_country = {
				change_government = tribal_chiefdom
				set_primary_culture = gutian
				set_country_religion = mesopotamian_religion
				#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
				#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
			}
		}
		p:6435 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:6435.owner
				}
			}
		}
		p:6253 = {
			set_conquered_by = p:6435.owner
		}
		p:6260 = {
			set_conquered_by = p:6435.owner
		}
		p:6217 = { #Dur-Papsukkal
	    	create_country = {
				change_government = despotic_monarchy
				set_primary_culture = south_akkadian
				set_country_religion = mesopotamian_religion
				#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
				#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
			}
		}
		p:6217 = {
			area = {
				every_area_province = {
					limit = {
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:6217.owner
				}
			}
		}
		p:6224 = {
			set_conquered_by = p:6217.owner
		}
		p:6215 = {
			set_conquered_by = p:6217.owner
		}
		p:5607 = {
			set_conquered_by = p:6217.owner
		}
		p:1993 = { #Fourth Migratory Tribe
	    	create_country = {
				change_government = tribal_chiefdom
				set_primary_culture = gutian
				set_country_religion = mesopotamian_religion
				#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
				#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
			}
		}
		p:1993 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:1993.owner
				}
			}
		}
		p:5606 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:1993.owner
				}
			}
		}
		p:6194 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:1993.owner
				}
			}
		}
		p:5893 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:5594.owner
				}
			}
		}
		p:6258 = {
			area = {
				every_area_province = {
					limit = { 
						has_owner = yes
						trigger_if = {
							limit = { has_owner = yes }
							owner = {
								tag = GTM
							}
						}
					}
					set_conquered_by = p:6254.owner
				}
			}
		}
		c:GTM = {
			every_owned_province = {
				set_conquered_by = p:1993.owner
			}
		}
		every_country = {
			limit = {
				NOR = {
					country_culture_group = aegean
					country_culture_group = hellenic
					country_culture_group = bryges
					country_culture_group = luwian
					country_culture_group = berber
					country_culture_group = nubian
				}
			}
			trigger_event = {
				id = ba_gutium.3
			}
		}
    }
}
ba_gutium.2 = { #Gutium Win
    type = country_event
    hidden = yes


    immediate = {
    	c:GTM = {
    		make_subject = {
		    	target = c:URR
		       	type = client_state
			}
			make_subject = {
		    	target = c:MMA
		       	type = client_state
			}
			make_subject = {
		    	target = c:SNN
		       	type = client_state
			}
			make_subject = {
		    	target = c:URK
		       	type = client_state
			}
			make_subject = {
		    	target = c:ADB
		       	type = client_state
			}
			make_subject = {
		    	target = c:DER
		       	type = client_state
			}
			
			#
			c:DBT = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:APL = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:MHR = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:MRD = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:KSH = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:BBL = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:SPR = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:KTH = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:MGM = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:RPQ = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
			c:KGL = {
				every_owned_province = {
					set_conquered_by = c:GTM
				}
			}
		}
		every_country = {
			limit = {
				NOR = {
					country_culture_group = aegean
					country_culture_group = hellenic
					country_culture_group = bryges
					country_culture_group = luwian
					country_culture_group = berber
					country_culture_group = nubian
				}
			}
			trigger_event = {
				id = ba_gutium.4
			}
		}
    }
}
ba_gutium.3 = { #Gutium Lose
    type = country_event
    title = ba_gutium.3.t
    desc = ba_gutium.3.desc
    picture = mesopotamian_empire
    
    left_portrait = current_ruler

    trigger = {
    
    }

    immediate = {
    	custom_tooltip = "GUTIUM_SHATTERED_TT"
    }

    option = {      
        name = ba_gutium.3.a
    }
}
ba_gutium.4 = { #Gutium Win
    type = country_event
    title = ba_gutium.4.t
    desc = ba_gutium.4.desc
    picture = mesopotamian_court
    
    left_portrait = current_ruler

    trigger = {
    
    }

    immediate = {
    	custom_tooltip = "GUTIUM_RESURGENT_TT"
    }

    option = {      
        name = ba_gutium.4.a
    }
}

ba_gutium.5 = { #Gutium Win
    type = country_event
    hidden = yes


    immediate = {
    	c:URK = {
    		make_subject = {
		    	target = c:GTM
		       	type = client_state
			}
		}
		hidden_effect = {
			p:6217 = { #Dur-Papsukkal
				create_country = {
					change_government = despotic_monarchy
					set_primary_culture = south_akkadian
					set_country_religion = mesopotamian_religion
					#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
					#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
				}
			}
			p:6217 = {
				area = {
					every_area_province = {
						limit = {
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:6217.owner
					}
				}
			}
			p:6224 = {
				set_conquered_by = p:6217.owner
			}
			p:6215 = {
				set_conquered_by = p:6217.owner
			}
			p:5607 = {
				set_conquered_by = p:6217.owner
			}
			p:1993 = { #Fourth Migratory Tribe
				create_country = {
					change_government = tribal_chiefdom
					set_primary_culture = gutian
					set_country_religion = mesopotamian_religion
					#change_country_name = "GUTIAN_MIGRATORY_TWO_NAME"
					#change_country_adjective = "GUTIAN_MIGRATORY_TWO_ADJECTIVE"
				}
			}
			p:1993 = {
				area = {
					every_area_province = {
						limit = { 
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:1993.owner
					}
				}
			}
			p:5606 = {
				area = {
					every_area_province = {
						limit = { 
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:1993.owner
					}
				}
			}
			p:6194 = {
				area = {
					every_area_province = {
						limit = { 
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:1993.owner
					}
				}
			}
			p:5893 = {
				area = {
					every_area_province = {
						limit = { 
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:5594.owner
					}
				}
			}
			p:6258 = {
				area = {
					every_area_province = {
						limit = { 
							has_owner = yes
							trigger_if = {
								limit = { has_owner = yes }
								owner = {
									tag = GTM
								}
							}
						}
						set_conquered_by = p:6254.owner
					}
				}
			}
		}
		every_country = {
			limit = {
				NOR = {
					country_culture_group = aegean
					country_culture_group = hellenic
					country_culture_group = bryges
					country_culture_group = luwian
					country_culture_group = berber
					country_culture_group = nubian
				}
			}
			trigger_event = {
				id = ba_gutium.4
			}
		}
    }
}