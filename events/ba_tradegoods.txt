﻿namespace = ba_tradegoods

ba_tradegoods.1 = {
	type = country_event
	title = "ba_tradegoods.1.t"
	desc = "ba_tradegoods.1.desc"
	picture = hellenic_marketplace

	immediate = {
		remove_country_modifier = has_bronze
	}
	option = {
		#nothing but tooltip
	}
}
ba_tradegoods.2 = {
	type = country_event
	title = "ba_tradegoods.2.t"
	desc = "ba_tradegoods.2.desc"
	picture = hellenic_marketplace

	immediate = {
		add_country_modifier = {
		name = "has_bronze"
		duration = -1
		}
	}
	option = {
		#nothing but tooltip
	}
}