﻿1246={ #Walwalla
	terrain="hills"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="fish"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1323={ #Passa
	terrain="forest"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1405={ #Pertassa
	terrain="farmland"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1504={ #Kapriala
	terrain="forest"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1505={ #Ikra
	terrain="mountain"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=3
	}
}

1524={ #Jalot
	terrain="deep_forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=4
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=3
	}
}

1526={ #Vorvia
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="stone"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1573={ #Iso
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="olive"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1578={ #Otro
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1582={ #Dranok
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1583={ #Urkri
	terrain="deep_forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=14
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1584={ #Ute
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="base_metals"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1585={ #Pefir
	terrain="mountain"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=19
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1586={ #Ekir
	terrain="deep_forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1587={ #Otan
	terrain="mountain"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wild_game"
	civilization_value=4
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1590={ #Moktriani
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

1591={ #Pharvarlo
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1592={ #Ziknik
	terrain="forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="fur"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1593={ #Ire
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1594={ #Irsikexa
	terrain="deep_forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=4
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1595={ #Orsolone
	terrain="mountain"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="fur"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1596={ #Alropana
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1598={ #Ero
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1599={ #Sallawassa
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="olive"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1601={ #Amauni
	terrain="forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1603={ #Wanona
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=23
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1605={ #Arwo
	terrain="forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1607={ #Wanaraha
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1608={ #Afaxra
	terrain="deep_forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=4
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

1610={ #Wumi
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="earthware"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

1611={ #Anarawa
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="olive"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1612={ #Iwauxa
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="copper"
	civilization_value=28
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1613={ #Izga
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1614={ #Arak
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1615={ #Azisso
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1616={ #Awirli
	terrain="forest"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="leather"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1617={ #Ula
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="stone"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1620={ #Kluri
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="stone"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1621={ #Iturwa
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="fur"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1622={ #Aniane
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

1623={ #Unuaku
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1628={ #Priku
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1630={ #Zilala
	terrain="mountain"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wild_game"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1632={ #Anykra
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1669={ #Unuro
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1670={ #Azez
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="olive"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1671={ #Priapu
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1684={ #Aktili
	terrain="mountain_valley"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=1
	}
}

1685={ #Anrandu
	terrain="plains"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="cloth"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=1
	}
}

2067={ #Huyassa
	terrain="mountain_valley"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="stone"
	civilization_value=14
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2350={ #Ikkutta
	terrain="farmland"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

2434={ #Warsa
	terrain="deep_forest"
	culture="wilusan"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=19
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2492={ #Walitta
	terrain="forest"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wood"
	civilization_value=9
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2494={ #Wassari
	terrain="farmland"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2495={ #Huntiya
	terrain="farmland"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2496={ #Passitli
	terrain="forest"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2497={ #Wiyassa
	terrain="hills"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="leather"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2624={ #Wallarimma
	terrain="mountain_valley"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="cloth"
	civilization_value=50
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=3
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

3331={ #Lukrili
	terrain="mountain"
	culture="lukkan"
	religion="anatolian_religion"
	trade_goods="wild_game"
	civilization_value=4
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

