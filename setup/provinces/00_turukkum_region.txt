﻿5280={ #Takpirtu
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="dates"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
}

5281={ #Tabbatis
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="dates"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
}

5282={ #Unnutu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5283={ #Harmasu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5284={ #Zutlum
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

5355={ #Dummuqtu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5356={ #Buaru
	terrain="forest"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5393={ #Matimini
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5394={ #Haduti
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5395={ #Minatisu
	terrain="forest"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5396={ #Amaqtu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5398={ #Hiptuna
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=45
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

5399={ #Isete
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5439={ #Nishatu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=33
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5440={ #Namirtu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="wild_game"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5441={ #Musallu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5442={ #Siksabbum
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=59
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

5443={ #Limishu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5444={ #Kupartu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5445={ #Ishtartu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5453={ #Sare
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="honey"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5456={ #Baramu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="wild_game"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="tukri"
		religion="hurrian_religion"
		amount=2
	}
	tribesmen={
		culture="tukri"
		religion="hurrian_religion"
		amount=1
	}
	tribesmen={
		amount=1
	}
}

5457={ #Assanu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5458={ #Ardabu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cloth"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5459={ #Anaku
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5460={ #Alluttu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5461={ #Qabra
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cloth"
	civilization_value=50
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=3
	}
	slaves={
		amount=2
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5462={ #Akalutu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5463={ #Abarakkatu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5465={ #Senkurru
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="leather"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5466={ #Karani
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="leather"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5467={ #Reshutu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5468={ #Shamuktu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="precious_metals"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

5469={ #Shugitu
	terrain="mountain_valley"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="earthware"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5470={ #Tahtiptu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="honey"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="south_akkadian"
		religion="mesopotamian_religion"
		amount=3
	}
	tribesmen={
		amount=1
	}
}

5471={ #Shussar
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5472={ #Tullulu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5473={ #Tabish
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cloth"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	freemen={
		culture="south_akkadian"
		religion="mesopotamian_religion"
		amount=3
	}
	tribesmen={
		amount=1
	}
}

5474={ #Ubaru
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="fur"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5475={ #Ushmittu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cloth"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5476={ #Wabrum
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5477={ #Zimbanu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="dates"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5478={ #Arrapha
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="dates"
	civilization_value=50
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=7
	}
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=4
	}
	freemen={
		amount=3
	}
	slaves={
		amount=3
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
}

5479={ #Napraru
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="fur"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5480={ #Ahazum
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=45
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5481={ #Naklish
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cloth"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5482={ #Masiktu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=28
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5483={ #Labbunu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="earthware"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5484={ #Arzuhina
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="fur"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5485={ #Dur-Atanate
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5486={ #Sahitu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="fur"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5487={ #Shudamelum
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="fur"
	civilization_value=45
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5488={ #Kitturu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5489={ #Babiti
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="leather"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5491={ #Ishku
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5501={ #Burumu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5510={ #Kunshu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=33
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5511={ #Lubushu
	terrain="plains"
	culture="assyrian"
	religion="mesopotamian_religion"
	trade_goods="wild_game"
	civilization_value=33
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5529={ #Qarrishu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5530={ #Pilanish
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5533={ #Nidutu
	terrain="plains"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="earthware"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="assyrian"
		religion="mesopotamian_religion"
		amount=1
	}
}

5610={ #Habâlu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="earthware"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5611={ #Dâsu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5612={ #Egêru
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5613={ #Hamâsu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5614={ #Kurbailu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=9
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5615={ #Lillatu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=9
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

5616={ #Qulâlu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5617={ #Taslîtu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5618={ #Atidu
	terrain="forest"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

5619={ #Musi
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

5620={ #Sashtu
	terrain="mountain_valley"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5621={ #Samâku
	terrain="mountain_valley"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wild_game"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5622={ #Lemnutu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="copper"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

5623={ #Harrania
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
}

5624={ #Washâtu
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
}

5625={ #Mumbium
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5626={ #Naiâkûtu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5627={ #Shusharra
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="grain"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=6
	}
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

5628={ #Kulunnum
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
}

5629={ #Hatû
	terrain="farmland"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
}

5630={ #Egru
	terrain="mountain_valley"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="wood"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

5631={ #Dastu
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5632={ #Shêtu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=2
	}
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		religion="mesopotamian_religion"
		amount=1
	}
}

5633={ #Gilâmu
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5634={ #Arragdi
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="wild_game"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

5635={ #Azari
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

5636={ #Bâsish
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6453={ #Mudi
	terrain="hills"
	culture="lullubi"
	religion="mesopotamian_religion"
	trade_goods="wild_game"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6674={ #Qissu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6736={ #Tanîpu
	terrain="hills"
	culture="tukri"
	religion="hurrian_religion"
	trade_goods="stone"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

