﻿10={ #Huliam
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

27={ #Shaltu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

52={ #Pashi
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

67={ #Shilati
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

119={ #Elamkû
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

145={ #Hutnû
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

179={ #Ahâtûtu
	terrain="desert"
	culture="sumerian"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

193={ #Zirkuh
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

213={ #Hashadu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

473={ #Dashpu
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

511={ #Riqqû
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

1453={ #Nîru
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1545={ #Mîru
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1563={ #Shummannu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1653={ #Tell Abraq
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=5
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1730={ #Adîrish
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1799={ #Umm an-Nar
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1804={ #Sirri
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1807={ #Ubbubu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1809={ #Arakata
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="spices"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1826={ #Shakâru
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1839={ #Sha'qâti
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1851={ #Napîshu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1995={ #Failaka
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
	holy_site = omen_inzak
}

2074={ #Forur
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=10
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2076={ #Moussa
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="spices"
	civilization_value=10
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

2082={ #Huzzû
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=10
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

2091={ #Nu'ayr
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2092={ #Purîdu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=10
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2111={ #Qablu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2139={ #Pêrtu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2141={ #Das
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="spices"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2159={ #Labiânu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2191={ #Shaptu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2204={ #Delma
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2410={ #Pûqu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

2425={ #Pêmu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

2429={ #Pappât
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2490={ #Targumu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

2637={ #Yas
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

5666={ #Dilmun
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=6
	}
	freemen={
		amount=8
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=2
	}
	holy_site = omen_meskilak
}

6716={ #Qitrubu
	terrain="plains"
	culture="sumerian"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6717={ #Labasu
	terrain="plains"
	culture="sumerian"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6831={ #Hashûr
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6832={ #Harbutu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6833={ #Kullu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6834={ #Nashpaku
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="dye"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6835={ #Kurillu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6836={ #Kalkaltu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6837={ #Gissish
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6838={ #Dalû
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6839={ #Aqqullu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6840={ #Nassishu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="dye"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6841={ #Sikkatânu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6842={ #Mishlâ
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6843={ #Manât
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6844={ #Ita
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6845={ #Para'u
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6846={ #Nakmartu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6847={ #Summunu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6848={ #Misissa
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6849={ #Igibû
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6850={ #Errê
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6851={ #Sarâtu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6852={ #Shuplu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

6853={ #Uldu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6854={ #Zâzu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6855={ #Rahû
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6856={ #Muttarrittum
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6858={ #Tarut
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

6859={ #Naiabtu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="stone"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6861={ #Halâbu
	terrain="hills"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6862={ #Bâmtu
	terrain="hills"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="copper"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

6863={ #Nasku
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6864={ #Bashmu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6865={ #Serru
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6866={ #Qubirtu
	terrain="hills"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="copper"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

6867={ #Hulmâhu
	terrain="hills"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="copper"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6868={ #Shammanu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6869={ #Urnu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6870={ #Pashâlu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6871={ #Kussimtu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6872={ #Alittu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6873={ #Parâshu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6874={ #Nûnu
	terrain="desert"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6875={ #Enqêtu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6876={ #Ersuppu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="gems"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6877={ #Nûni
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="copper"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6878={ #Al-Ain
	terrain="oasis"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="dates"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6879={ #Hili
	terrain="oasis"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="dates"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=5
	}
	freemen={
		amount=7
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6880={ #Sillatu
	terrain="plains"
	culture="maganite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6881={ #Isqillatu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6882={ #Saiâdu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6883={ #Yalu
	terrain="plains"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

6884={ #Uridimmu
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fruits"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=3
	}
}

6885={ #Urmahu
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fruits"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=3
	}
}

6886={ #Ya'alu
	terrain="oasis"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="dates"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=3
	}
}

6887={ #Gerru
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6888={ #Arnabtu
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6889={ #Harriru
	terrain="desert"
	culture="dilmunite"
	religion="mesopotamian_religion"
	trade_goods="fish"
	civilization_value=19
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

