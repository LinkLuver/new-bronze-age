﻿"Afterworld_URK"={
	country="URK"

	15={
		first_name="Gilgamesh" # Gilgamesh
		family_name="of Uruk"
		birth_date=398.7.1
		death_date=433.6.1
		culture="sumerian"
		religion="mesopotamian_religion"
		no_stats=yes
		add_martial=21
		add_charisma=25
		add_finesse=18
		add_zeal=15
		no_traits=yes
		dna="Ff8Z+Y+Vj5Xl/6rhApgCmAJ/An8ClwKXAmgCaAKDAoMCmAKYAnsCewJuAm4CdgJ2AooCigKZApkBUAFQAoUChQKNAo0CiAKIAoECgQOoA6gCeQJ5AogCiAJ3AncCegJ6A4wDjAKIAogCkAKQAnACcAJ/An8CaQJpAm0CbQKXApcChwKHAoUChQKDAoMCfQJ9AosCiwJ2AnYCbQJtApACkAKFAoUCagJqApMCkwJ5AnkCdwJ3AnsCewKZApkCeQJ5A6kDqQJwAnACaAJoAoQChAFUAVQCewJ7ApcClwKHAocCegJ6AocChwCeAJ4AvAC8AGgAaAK/Ar8A0gDSANEA0QFtAW0BVQFVAB0AHQBiAGIDQANAABcAFwAAAAAAsACwAKUApQAbABsAxQDFAXABcAKMAowDSgNKAMgAyAAAAAABQQFBAugC6AAmACYA/AD8ACcAJwFjAWMAmACYAHUAdQF0AXQA2QDZASYBJgMTAxMMjQaRABAAAAX/BToSKBJmAfgB+Ar/CnMRzhBbCiYD/QAAAAAAAAAAAAAAAAAAAAA="
	}
}
"Afterworld_KSH"={
	country="KSH"

	17={
		first_name="Kubau" # Kubau/Kubaba/Hepat
		family_name="of Kish"
		birth_date=400.7.1
		death_date=500.6.1
		culture="south_akkadian"
		religion="mesopotamian_religion"
		female=yes
		no_stats=yes
		add_martial=5
		add_charisma=25
		add_finesse=27
		add_zeal=12
		no_traits=yes
		dna="vHy8fJ+Qn5DPxc/FAnkCeQKRApECjQKNAnsCewKRApECfAJ8AmkCaQJ+An4CawJrApMCkwKDAoMCngKeAncCdwOxA7ECawJrAnoCegKAAoACbwJvAngCeAJ/An8CfAJ8AoUChQJ3AncBVwFXAWIBYgKTApMChQKFApICkgKSApICawJrAncCdwKEAoQCZwJnAogCiAKEAoQCfQJ9AoACgAKXApcCaAJoAmkCaQJ1AnUChwKHApACkAKNAo0CkQKRAnECcQJ/An8CgQKBAncCdwKUApQCgwKDADcANwOvA68CbgJuAmcCZwP6A/oAJQAlApICkgBGAEYBHQEdAq8CrwFhAWEBbAFsADsAOwFrAWsDBQMFACoAKgAAAAAAMQAxAAMAAwBIAEgA8ADwAXMBcwBuAG4AiQCJAJgAmAAAAAAAQABAAX8BfwB6AHoAswCzAOQA5AERAREBOwE7Az8DPwD7APsBIwEjBJkEmQNMA0wKTgpOAAAAAAUABQAPWw9bAUUBRQIlAiUMAQwBA3wDfAAAAAAAAAAAAAAAAAAAAAA="
	}
}
"Afterworld_GTM"={
	country="GTM"

	18={
		first_name="Sargon" # Sargon
		family_name="of Akkad"
		birth_date=566.7.23
		death_date=616.6.12
		culture="south_akkadian"
		religion="mesopotamian_religion"
		no_stats=yes
		add_martial=25
		add_charisma=16
		add_finesse=18
		add_zeal=15
		no_traits=yes
		dna="QOVA5Z6Jnonsy+zLAVUBVQKDAoMChAKEApUClQJ1AnUDpQOlAmkCaQJ5AnkCfwJ/AncCdwJsAmwCoAKgAocChwFTAVMCeQJ5AnkCeQKAAoACjQKNAooCigJ+An4CdAJ0ApcClwJzAnMCcgJyAAcABwKSApICjQKNAnsCewKUApQCbwJvApkCmQJzAnMCbAJsAosCiwJ9An0CagJqAnsCewJ1AnUCcQJxAoECgQJpAmkCkgKSAnsCewKKAooCiAKIApkCmQKDAoMCbAJsAooCigFWAVYClgKWAVUBVQKTApMCjwKPAooCigKOAo4BGAEYAJAAkAAyADIA2QDZAXoBegCxALEBDAEMAG8AbwF3AXcDbQNtALcAtwAAAAABEQERAQgBCADRANEBLQEtAB8AHwBVAFUAmQCZAoUChQAAAAACzgLOAEwATAKvAq8ABQAFAN4A3gCDAIMAJwAnAzADMAAdAB0DRQNFAToBOgECAQIE/QT9AAAAAAUbBRsPdQ91AbgBuAbiBuIIPwg/DEsMSwAAAAAAAAAAAAAAAAAAAAA="
	}
}