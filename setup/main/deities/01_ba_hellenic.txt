deity_manager = {
	deities_database = { ### KEYS 900-999 ###
		# Hellenic Deities
		900 = {
			key = omen_aiolos
			deity = deity_aiolos
		}
		901 = {
			key = omen_daidalos
			deity = deity_daidalos
		}
		902 = {
			key = omen_deiwos
			deity = deity_deiwos
		}
		903 = {
			key = omen_dioskouri
			deity = deity_dioskouri
		}
		904 = {
			key = omen_diwa
			deity = deity_diwa
		}
		905 = {
			key = omen_doros
			deity = deity_doros
		}
		906 = {
			key = omen_drimios
			deity = deity_drimios
		}
		907 = {
			key = omen_enyalios
			deity = deity_enyalios
		}
		908 = {
			key = omen_herakles
			deity = deity_herakles
		}
		909 = {
			key = omen_ion
			deity = deity_ion
		}
		910 = {
			key = omen_kronos
			deity = deity_kronos
		}
		911 = {
			key = omen_matere_ga
			deity = deity_matere_ga
		}
		912 = {
			key = omen_paiaon
			deity = deity_paiaon
		}
		913 = {
			key = omen_sito_potnia
			deity = deity_sito_potnia
		}
		914 = {
			key = omen_worsanos
			deity = deity_worsanos
		}
	}
} 