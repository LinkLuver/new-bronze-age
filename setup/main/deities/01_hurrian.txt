deity_manager = {
	deities_database = { ### KEYS 600-799 ###
		# Hurrian Deities
		600 = {
			key = omen_teshub
			deity = deity_teshub
		}
		601 = {
			key = omen_kumarbi
			deity = deity_kumarbi
		}
		602 = {
			key = omen_shaushka
			deity = deity_shaushka
		}
		603 = {
			key = omen_hebat
			deity = deity_hebat
		}
		604 = {
			key = omen_sharruma
			deity = deity_sharruma
		}
		605 = {
			key = omen_shimegi
			deity = deity_shimegi
		}
		606 = {
			key = omen_kushuh
			deity = deity_kushuh
		}
		607 = {
			key = omen_nabarbi
			deity = deity_nabarbi
		}
		608 = {
			key = omen_hutena_hutelluri
			deity = deity_hutena_hutelluri
		}
		609 = {
			key = omen_sumuqan
			deity = deity_sumuqan
		}
		610 = {
			key = omen_ashtabil
			deity = deity_ashtabil
		}
		611 = {
			key = omen_hayya
			deity = deity_hayya
		}
		612 = {
			key = omen_hurrian_ningal
			deity = deity_hurrian_ningal
		}
		613 = {
			key = omen_sherri_hurri
			deity = deity_sherri_hurri
		}
		614 = {
			key = omen_shuwaliyatti
			deity = deity_shuwaliyatti
		}
		615 = {
			key = omen_tasmisu
			deity = deity_tasmisu
		}
		616 = {
			key = omen_kubaba
			deity = deity_kubaba
		}
	}
} 